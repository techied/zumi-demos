import socket

import requests

multicast_addr = '224.1.1.1'
bind_addr = '0.0.0.0'
port = 5007

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
membership = socket.inet_aton(multicast_addr) + socket.inet_aton(bind_addr)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind((bind_addr, port))

while True:
    message, address = sock.recvfrom(255)
    decoded = message.decode('utf-8')
    print(address, decoded)
    if decoded == 'teacher':
        send_address = address[0]
        break

r = requests.post("http://" + send_address + ":8081/upload", data={'code': """
from zumi.personality import Screen

screen = Screen()
screen.draw_text_center('this is a test!')"""})
print(r.status_code, r.reason)
print(r.text[:300] + '...')
