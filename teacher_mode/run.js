let endpoint = window.location.protocol + "//" + window.location.hostname;
window.socket = io(endpoint);
window.socketConnected = true;
window.overlayDisconnectionDisable = false;
window.socket.on("disconnect", () => {
    window.socketConnected = false;
    console.log("disconnected at: ", new Date().toString());
    window.timeoutSocketConnectionLost = setTimeout(function () {
        if (!window.overlayDisconnectionDisable) {
            document.getElementsByClassName("global-overlay")[0].style.display =
                "initial";
        }
    }, 1500);
});
window.socket.on("reconnect", attemptNumber => {
    window.socketConnected = true;
    clearTimeout(window.timeoutSocketConnectionLost);
    document.getElementsByClassName("global-overlay")[0].style.display = "none";
});

document.getElementById("scan").addEventListener('click', async () => {
    console.log("sending scan");
    var encodedString = window.btoa(unescape(encodeURIComponent(textToSave)));
    window.socket.emit("scan", encoded_string);
});
