import time

import cv2
from zumi.personality import Personality
from zumi.util.screen import Screen
from zumi.zumi import Zumi

zumi = Zumi()
screen = Screen()
personality = Personality(zumi, screen)

width = 160
height = 120

cap = cv2.VideoCapture(0)

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 160)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 120)

# Color definitions, to pull an object out of the frame [hue, sat, val, hrange, srange, vrange]
color_definitions = [8, 255, 255, 36, 54, 127]


# /////////////////////////////
# Define a function that filters the desired color, takes in a frame as an argument
# READ MORE: https://docs.python.org/3/tutorial/controlflow.html#defining-functions
def filter_color(frame):
    # Convert the captured frame to
    # "Hue Saturation Value" (HSV) color space from "Blue Green Red" (BGR) color space
    # READ MORE: http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html
    # READ MORE: https://en.wikipedia.org/wiki/HSL_and_HSV
    # ***************In OpenCV Hue range is from 0-180 and not from 0 to 360********************
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # /////////////////////////////

    hue = color_definitions[0]
    sat = color_definitions[1]
    val = color_definitions[2]
    hrange = color_definitions[3]
    srange = color_definitions[4]
    vrange = color_definitions[5]

    # Create a boundary for any color that gets updated by trackbars
    # READ MORE: https://www.pyimagesearch.com/2014/08/04/opencv-python-color-detection/
    color_lower = (hue - hrange, sat - srange, val - vrange)
    color_upper = (hue + hrange, sat + srange, val + vrange)
    # ( Hue 0-180, Saturation 0-255, Value 0-255 )
    # EXAMPLE
    # Say the user moves the trackbar and sets the hue to 10 and hrange to 10
    # The lower hue bound will be 10-10 = 0 and upper hue bound will be 10+10=20.

    # /////////////////////////////

    # This returns a binary (black and white) frame and assigns it to 'filtered frame'
    # Any white on the frame is the color we are detecting
    # READ MORE: https://www.pyimagesearch.com/2014/08/04/opencv-python-color-detection/
    filtered_frame = cv2.inRange(hsv, color_lower, color_upper)

    # Uncomment the line below to see the binary frame
    # cv2.imshow('filtered_frame', filtered_frame)

    # This superimposes the black and white frame on top of the original frame to let the desired color through
    # Assigned to a new frame 'color_cutout'
    # color_cutout = cv2.bitwise_and(frame, frame, mask=filtered_frame)
    # Note: This function returns the binary (black and white) frame
    return filtered_frame


# Function that returns the array of points for the biggest contour
# Takes in a binary (black and white) frame as its argument
def find_biggest_contour(mask):
    # Contours
    # READ MORE: https://docs.opencv.org/3.1.0/d4/d73/tutorial_py_contours_begin.html
    # READ MORE: https://docs.opencv.org/3.1.0/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a

    # Create an array of contour points
    # READ MORE: https://docs.opencv.org/3.1.0/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a
    contours = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)[-2]

    # This code will execute if at least one contour was found
    if len(contours) > 0:
        # Find the biggest contour
        biggest_contour = max(contours, key=cv2.contourArea)
        # Returns an array of points for the biggest contour found
        return biggest_contour


def drive_with_position(zumi, x, box_width, width, tolerance):
    center = width / 2
    center_diff = (x + (w / 2)) - center
    print('diff: ' + str(center_diff))
    if center_diff == -(width / 2):
        zumi.stop()
    elif center_diff > tolerance:
        zumi.control_motors(1, 30, 20)
    elif center_diff < -tolerance:
        zumi.control_motors(10, 1, 20)
    else:
        zumi.control_motors(1, 1, 20)


try:
    while True:
        time_start = time.time()
        ret, frame = cap.read()
        cv2.flip(frame, 0, frame)
        mask = filter_color(frame)
        contour = find_biggest_contour(mask)
        x, y, w, h = cv2.boundingRect(contour)
        drive_with_position(zumi, x, w, width, 15)
        print("FPS" + str(1 / (time.time() - time_start)))

finally:
    cap.release()

# In[ ]:
