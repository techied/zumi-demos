from random import randint

from PIL import Image

import Adafruit_SSD1306
from zumi.zumi import Zumi


def get_dice_object(number):
    if number == 1:
        return one
    if number == 2:
        return two
    if number == 3:
        return three
    if number == 4:
        return four
    if number == 5:
        return five
    if number == 6:
        return six
    return one


zumi = Zumi()

sensor_data = zumi.update_angles()
rot_sens = 20
accel_sens = 150
old_x = sensor_data[0]
old_y = sensor_data[1]
old_z = sensor_data[2]
old_accel_x = sensor_data[3]
old_accel_y = sensor_data[4]
print(sensor_data)
prefix = 'dice/'
one = Image.open(prefix + 'one.ppm')
two = Image.open(prefix + 'two.ppm')
three = Image.open(prefix + 'three.ppm')
four = Image.open(prefix + 'four.ppm')
five = Image.open(prefix + 'five.ppm')
six = Image.open(prefix + 'six.ppm')
try:
    disp = Adafruit_SSD1306.SSD1306_128_64(rst=24)
    disp.begin()
    disp.clear()
    disp.display()
    width = disp.width
    height = disp.height

except Exception as e:
    print(e)

print("ready")
while True:
    sensor_data = zumi.update_angles()
    x = sensor_data[0]
    y = sensor_data[1]
    z = sensor_data[2]
    accel_x = sensor_data[3]
    accel_y = sensor_data[4]
    if abs(x - old_x) > rot_sens or abs(y - old_y) > rot_sens or abs(z - old_z) > rot_sens or abs(
            accel_x - old_accel_x) > accel_sens or abs(accel_y - old_accel_y) > accel_sens:
        print("shaken")
        zumi.play_note(50, 100)
        image = Image.new('1', (width, height))
        image.paste(get_dice_object(randint(1, 6)), (0, 2))
        image.paste(get_dice_object(randint(1, 6)), (66, 2))
        disp.image(image)
        disp.display()
    old_x = x
    old_y = y
    old_z = z
    old_accel_x = accel_x
    old_accel_y = accel_y
