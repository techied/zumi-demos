import socket


def multicast_send(message):
    sock.sendto(message.encode('utf-8'), (MCAST_GRP, MCAST_PORT))


MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)

multicast_send('hello, world!')
