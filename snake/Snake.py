from random import randint

import sys
import time
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import Adafruit_SSD1306
from zumi.zumi import Zumi

RST = 24
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0
zumi = Zumi()
leftPressed = False
rightPressed = False
try:
    disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)
    disp.begin()
    disp.clear()
    disp.display()
    width = disp.width
    height = disp.height
except Exception as e:
    print(e)


class Field:
    def __init__(self, size_x, size_y):
        self.sizeX = size_x
        self.sizeY = size_y
        self.icons = {
            0: ' . ',
            1: ' * ',
            2: ' # ',
            3: ' & ',
        }
        self.snake_coords = []
        self._generate_field()
        self.add_entity()

    def add_entity(self):

        while True:
            i = randint(0, self.sizeX - 1)
            j = randint(0, self.sizeY - 1)
            entity = [i, j]

            if entity not in self.snake_coords:
                self.field[i][j] = 3
                break

    def _generate_field(self):
        self.field = [[0 for j in range(self.sizeY)] for i in range(self.sizeX)]

    def _clear_field(self):
        self.field = [[j if j != 1 and j != 2 else 0 for j in i] for i in self.field]

    def render(self):
        size_x = self.sizeX
        size_y = self.sizeY
        self._clear_field()

        # Render snake on the field
        for i, j in self.snake_coords:
            self.field[i][j] = 1

        # Mark head
        head = self.snake_coords[-1]
        self.field[head[0]][head[1]] = 1

        for i in range(size_x):
            row = ''
            for j in range(size_y):
                row += self.icons[self.field[i][j]]

    def get_entity_pos(self):
        for i in range(self.sizeX):
            for j in range(self.sizeY):
                if self.field[i][j] == 3:
                    return [i, j]

        return [-1, -1]

    def is_snake_eat_entity(self):
        entity = self.get_entity_pos()
        head = self.snake_coords[-1]
        return entity == head


class Snake:
    def __init__(self):
        self.direction = 'RIGHT'

        # Init basic coords
        self.coords = [[0, 0], [0, 1], [0, 2], [0, 3]]

    def turn_left(self):

        if self.direction == 'LEFT':
            self.direction = 'UP'
        elif self.direction == 'DOWN':
            self.direction = 'LEFT'
        elif self.direction == 'RIGHT':
            self.direction = 'DOWN'
        elif self.direction == 'UP':
            self.direction = 'RIGHT'

    def turn_right(self):
        if self.direction == 'LEFT':
            self.direction = 'DOWN'
        elif self.direction == 'DOWN':
            self.direction = 'RIGHT'
        elif self.direction == 'RIGHT':
            self.direction = 'UP'
        elif self.direction == 'UP':
            self.direction = 'LEFT'

    def level_up(self):
        # get last point direction
        a = self.coords[0]
        b = self.coords[1]

        tail = a[:]

        if a[0] < b[0]:
            tail[0] -= 1
        elif a[1] < b[1]:
            tail[1] -= 1
        elif a[0] > b[0]:
            tail[0] += 1
        elif a[1] > b[1]:
            tail[1] += 1

        tail = self._check_limit(tail)
        self.coords.insert(0, tail)

        # play level up sound
        zumi.play_note(note_type=48, note_duration=75)
        zumi.play_note(note_type=53, note_duration=525)

    def is_alive(self):
        head = self.coords[-1]
        snake_body = self.coords[:-1]
        return head not in snake_body

    def _check_limit(self, point):
        # Check field limit
        if point[0] > self.field.sizeX - 1:
            point[0] = 0
        elif point[0] < 0:
            point[0] = self.field.sizeX - 1
        elif point[1] < 0:
            point[1] = self.field.sizeY - 1
        elif point[1] > self.field.sizeY - 1:
            point[1] = 0

        return point

    def move(self):
        # Determine head coords
        head = self.coords[-1][:]

        # Calc new head coords
        if self.direction == 'UP':
            head[1] += 1
        elif self.direction == 'DOWN':
            head[1] -= 1
        elif self.direction == 'RIGHT':
            head[0] += 1
        elif self.direction == 'LEFT':
            head[0] -= 1
        print('pos: ' + str(head[0]) + ', ' + str(head[1]))

        # Check field limit
        head = self._check_limit(head)

        del (self.coords[0])
        self.coords.append(head)
        self.field.snake_coords = self.coords

        if not self.is_alive():
            zumi.play_note(note_type=51, note_duration=300)
            zumi.play_note(note_type=50, note_duration=300)
            zumi.play_note(note_type=49, note_duration=300)
            zumi.play_note(note_type=48, note_duration=800)
            font = ImageFont.truetype('futura.ttf', 16)
            image = Image.open('snake_end.ppm').convert('1')
            draw = ImageDraw.Draw(image)
            draw.text((90, 40), str(len(self.field.snake_coords)), font=font, fill=255)
            disp.image(image)
            disp.display()
            time.sleep(.5)
            sys.exit()

        if self.field.is_snake_eat_entity():
            self.level_up()
            self.field.add_entity()

    def set_field(self, field):
        self.field = field


def main():
    field = Field(32, 16)
    snake = Snake()
    snake.set_field(field)
    left_pressed = False
    right_pressed = False

    image = Image.open('snake_title.ppm').convert('1')
    disp.image(image)
    disp.display()

    while True:
        ir_readings = zumi.get_all_IR_data()
        left_ir = int(ir_readings[0])
        right_ir = int(ir_readings[5])
        if left_ir < 100:
            delay = 0.1  # Easy
            break
        if right_ir < 100:
            delay = 0  # Hard
            break

    disp.clear()
    disp.display()

    time.sleep(1)

    image = Image.new('1', (width, height))
    while True:
        ir_readings = zumi.get_all_IR_data()
        left_ir = int(ir_readings[0])
        right_ir = int(ir_readings[5])
        if left_ir < 100:
            if not left_pressed:
                left_pressed = True
                snake.turn_left()
            else:
                left_pressed = False
        if right_ir < 100:
            if not right_pressed:
                right_pressed = True
                snake.turn_right()
            else:
                right_pressed = False
        # Move snake
        try:
            snake.move()
        except Exception as exception:
            print(exception)
        # Render field
        field.render()
        pixels = image.load()
        copy = field.field.copy()

        for i in range(32):
            for j in range(16):
                pixels[i * 4, j * 4] = copy[i][j]
                pixels[i * 4, j * 4 + 1] = copy[i][j]
                pixels[i * 4, j * 4 + 2] = copy[i][j]
                pixels[i * 4, j * 4 + 3] = copy[i][j]

                pixels[i * 4 + 1, j * 4] = copy[i][j]
                pixels[i * 4 + 1, j * 4 + 1] = copy[i][j]
                pixels[i * 4 + 1, j * 4 + 2] = copy[i][j]
                pixels[i * 4 + 1, j * 4 + 3] = copy[i][j]

                pixels[i * 4 + 2, j * 4] = copy[i][j]
                pixels[i * 4 + 2, j * 4 + 1] = copy[i][j]
                pixels[i * 4 + 2, j * 4 + 2] = copy[i][j]
                pixels[i * 4 + 2, j * 4 + 3] = copy[i][j]

                pixels[i * 4 + 3, j * 4] = copy[i][j]
                pixels[i * 4 + 3, j * 4 + 1] = copy[i][j]
                pixels[i * 4 + 3, j * 4 + 2] = copy[i][j]
                pixels[i * 4 + 3, j * 4 + 3] = copy[i][j]

        disp.image(image)
        disp.display()

        time.sleep(delay)


if __name__ == '__main__':
    main()
