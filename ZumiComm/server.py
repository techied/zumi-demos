# from zumi.zumi import Zumi
# from zumi.util.screen import Screen
import socket
import time

hostName = "192.168.10.1"
hostPort = 8081
# zumi = Zumi()
# screen = Screen()
global tick
tick = 0


def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start + n]


message_split = chunks(message, 30)

print(message_split)


# class MyServer(BaseHTTPRequestHandler):
#     def do_GET(self):
#         self.send_response(200)
#         self.send_header("Content-type", "text/html")
#         self.end_headers()
# self.wfile.write(bytes("battery %: " + str(zumi.get_battery_percent()) + "<br>", "utf-8"))
# self.wfile.write(bytes("battery v: " + str(zumi.get_battery_voltage()), "utf-8"))

# def do_POST(self):


def multicast():
    for chunk in message_split:
        time.sleep(2)
        # threading.Timer(2, multicast).start()
        # global tick
        # tick = tick + 1
        sock.sendto(chunk.encode('utf-8'), (MCAST_GRP, MCAST_PORT))
        # screen.draw_text_center(lyrics_split[tick])


# myServer = HTTPServer((hostName, hostPort), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)

multicast()

# try:
#     myServer.serve_forever()
# except KeyboardInterrupt:
#     pass
#
# myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))
