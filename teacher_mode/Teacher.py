import socket
import threading
import time
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer

hostName = '192.168.1.87'  # socket.gethostbyname(socket.gethostname())
hostPort = 8081


class Server(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        print("GET request,\nPath: %s\nHeaders:\n%s\n" % (str(self.path), str(self.headers)))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        print("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n" % (
            str(self.path), str(self.headers), post_data.decode('utf-8')))

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        if self.path == '/inform':
            # TODO: this is where you display Zumi option to the dashboard
            pass
        elif self.path == '/upload':
            # TODO: this is where you display code review pending in dashboard
            # print(self.address_string())
            code = urllib.parse.unquote(post_data.decode('utf-8')).replace('+', ' ')[5:]
            print(code)
            exec(code)


def multicast():
    sock.sendto('teacher'.encode('utf-8'), (MCAST_GRP, MCAST_PORT))
    threading.Timer(2, multicast).start()


server = HTTPServer((hostName, hostPort), Server)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)

multicast()

try:
    server.serve_forever()
except KeyboardInterrupt:
    pass

server.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))
