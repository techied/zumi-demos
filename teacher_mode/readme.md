# Zumi Teach

1. Run teacher code on one Pi.
2. Run student code on any other Pi on the same WiFi network.
3. Watch as the code inside student.py is executed... on the teacher Pi!