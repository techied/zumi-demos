import asyncio
import logging
import socket
import time

from hbmqtt.client import MQTTClient, ClientException
from hbmqtt.mqtt.constants import QOS_1, QOS_2
from zumi.util.screen import Screen


@asyncio.coroutine
def uptime_coro():
    C = MQTTClient()
    yield from C.connect(mqtt_addr)
    # Subscribe to '$SYS/broker/uptime' with QOS=1
    yield from C.subscribe([
        ('$SYS/broker/uptime', QOS_1),
        ('$SYS/broker/load/#', QOS_2),
    ])
    logger.info("Subscribed")
    try:
        for i in range(1, 100):
            message = yield from C.deliver_message()
            packet = message.publish_packet
            print("%d: %s => %s" % (i, packet.variable_header.topic_name, str(packet.payload.data)))
        yield from C.unsubscribe(['$SYS/broker/uptime', '$SYS/broker/load/#'])
        logger.info("UnSubscribed")
        yield from C.disconnect()
    except ClientException as ce:
        logger.error("Client exception: %s" % ce)


logger = logging.getLogger(__name__)
current_milli_time = lambda: int(round(time.time() * 1000))
screen = Screen()
multicast_addr = '224.1.1.1'
bind_addr = '0.0.0.0'
port = 5007

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
membership = socket.inet_aton(multicast_addr) + socket.inet_aton(bind_addr)

sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

sock.bind((bind_addr, port))

time_start_listen = current_milli_time()

mqtt_addr = ''

while True:
    message, address = sock.recvfrom(255)
    print(message.decode('utf-8'), address)
    screen.draw_text_center(message.decode('utf-8'))
    if (message == 'zumi mqtt'):
        mqtt_addr = 'mqtt://' + address + '/'
        formatter = "[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s"
        logging.basicConfig(level=logging.INFO, format=formatter)
        asyncio.get_event_loop().run_until_complete(uptime_coro())
        break
