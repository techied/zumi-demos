import time
from random import randint

from PIL import Image, ImageDraw, ImageFont

import Adafruit_SSD1306
from zumi.zumi import Zumi
from zumi.util.screen import Screen
from zumi.personality import Personality

def wiggle():
    zumi.control_motors(10, -10, 20)
    time.sleep(0.3)
    zumi.control_motors(-10, 10, 20)
    time.sleep(0.3)
    zumi.control_motors(10, -10, 20)
    time.sleep(0.3)
    zumi.control_motors(-10, 10, 20)
    time.sleep(0.3)
    zumi.control_motors(0, 0, 20)

def wait_for_shake():
    sensor_data = zumi.update_angles()
    old_x = sensor_data[0]
    old_y = sensor_data[1]
    old_z = sensor_data[2]
    old_accel_x = sensor_data[3]
    old_accel_y = sensor_data[4]
    print('Waiting for shake')
    while True:
        sensor_data = zumi.update_angles()
        x = sensor_data[0]
        y = sensor_data[1]
        z = sensor_data[2]
        accel_x = sensor_data[3]
        accel_y = sensor_data[4]
        if abs(x - old_x) > rot_sens or abs(y - old_y) > rot_sens or abs(z - old_z) > rot_sens or abs(
                accel_x - old_accel_x) > accel_sens or abs(accel_y - old_accel_y) > accel_sens:
            print("Shook!")
            return
        old_x = x
        old_y = y
        old_z = z
        old_accel_x = accel_x
        old_accel_y = accel_y


def get_ir_input():
    print('Waiting for IR')
    while True:
        ir_readings = zumi.get_all_IR_data()
        left_ir = int(ir_readings[0])
        right_ir = int(ir_readings[5])
        if left_ir < 50 and right_ir < 50:
            continue
        if left_ir < 50:
            print('Pressed LEFT')
            return 'LEFT'
        if right_ir < 50:
            print('Pressed RIGHT')
            return 'RIGHT'


def get_dice_object(number):
    if number == 1:
        return one
    if number == 2:
        return two
    if number == 3:
        return three
    if number == 4:
        return four
    if number == 5:
        return five
    if number == 6:
        return six
    return one


zumi = Zumi()
screen = Screen()
personality = Personality(zumi, screen)
rot_sens = 20
accel_sens = 150
your_score_pos = (92, 25)
zumi_score_pos = (94, 22)
path = '21_dice/res/'
title = Image.open(path + 'dice_title.ppm').convert('1')
your_score_disp = Image.open(path + 'your_score.ppm').convert('1')
zumi_score_disp = Image.open(path + 'zumi_score.ppm').convert('1')
zumi_wins = Image.open(path + 'dice_zumi_wins.ppm').convert('1')
you_win = Image.open(path + 'dice_you_win.ppm').convert('1')
tie = Image.open(path + 'dice_tie.ppm').convert('1')
roll = Image.open(path + 'roll.ppm').convert('1')
set_flat = Image.open(path + 'set_flat.ppm').convert('1')
blank = Image.open(path + 'blank.ppm')
one = Image.open(path + 'one.ppm')
two = Image.open(path + 'two.ppm')
three = Image.open(path + 'three.ppm')
four = Image.open(path + 'four.ppm')
five = Image.open(path + 'five.ppm')
six = Image.open(path + 'six.ppm')
font = ImageFont.truetype(path + 'futura.ttf', 16)
try:
    disp = Adafruit_SSD1306.SSD1306_128_64(rst=24)
    disp.begin()
    disp.clear()
    disp.display()
    width = disp.width
    height = disp.height
except Exception as e:
    print(e)

print("ready")
disp.image(title)
disp.display()
get_ir_input()
disp.clear()
disp.display()
while True:
    skip = False
    your_score = 0
    zumi_score = 0

    while your_score <= 21:
        disp.image(roll)
        disp.display()
        wait_for_shake()
        dice_one = randint(1, 6)
        dice_two = randint(1, 6)
        your_score += dice_one
        your_score += dice_two
        image = Image.new('1', (width, height))
        image.paste(get_dice_object(dice_one), (0, 2))
        image.paste(get_dice_object(dice_two), (66, 2))
        disp.image(image)
        disp.display()
        zumi.play_note(50, 100)
        time.sleep(3)
        if your_score > 21:
            image = Image.new('1', (width, height))
            draw = ImageDraw.Draw(image)
            draw.text(your_score_pos, "You: " + str(your_score) + "\nZumi: " + str(zumi_score), font=font, fill=255)
            disp.image(image)
            time.sleep(3)
            personality.celebrate()
            disp.image(zumi_wins)
            disp.display()
            time.sleep(3)
            skip = True
            break
        image = your_score_disp.copy()
        draw = ImageDraw.Draw(image)
        draw.text(your_score_pos, str(your_score), font=font, fill=255)
        disp.image(image)
        disp.display()
        if get_ir_input() == 'LEFT':
            break
    if skip:
        if get_ir_input() == 'LEFT':
            personality.awake()
            quit()
        else:
            continue
    disp.image(set_flat)
    disp.display()
    time.sleep(3)
    while zumi_score < 17:
        wiggle()
        image = Image.new('1', (width, height))
        dice_one = randint(1, 6)
        dice_two = randint(1, 6)
        zumi_score += dice_one
        zumi_score += dice_two
        image.paste(get_dice_object(dice_one), (0, 2))
        image.paste(get_dice_object(dice_two), (66, 2))
        disp.image(image)
        disp.display()
        zumi.play_note(50, 100)
        time.sleep(3)
        image = zumi_score_disp.copy()
        draw = ImageDraw.Draw(image)
        draw.text(zumi_score_pos, str(zumi_score), font=font, fill=255)
        disp.image(image)
        disp.display()
        time.sleep(3)
    if zumi_score > 21:
        disp.image(you_win)
    else:
        if your_score > zumi_score:
            disp.image(you_win)
        elif zumi_score > your_score:
            disp.image(zumi_wins)
            personality.celebrate()
        else:
            disp.image(tie)
    disp.display()
    if get_ir_input() == 'LEFT':
        personality.awake()
        quit()
